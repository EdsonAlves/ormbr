{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.mapping.explorer;

interface

uses
  Classes,
  SysUtils,
  Rtti,
  DB,
  GpSQLBuilder,
  Generics.Collections,
  /// orm
  ormbr.mapping.attributes,
  ormbr.mapping.classes,
  ormbr.mapping.rttiutils,
  ormbr.mapping.popular,
  ormbr.mapping.explorerstrategy,
  ormbr.mapping.repository,
  ormbr.mapping.register;

type
  TMappingExplorer = class(TMappingExplorerStrategy)
  private
  class var
    FInstance: TMappingExplorer;
  private
    FRepositoryMapping: TMappingRepository;
    FPopularMapping: TMappingPopular;
    FTableMapping: TDictionary<string, TTableMapping>;
    FPrimaryKeyMapping: TDictionary<string, TPrimaryKeyMappingList>;
    FForeingnKeyMapping: TDictionary<string, TForeignKeyMappingList>;
    FIndexeMapping: TDictionary<string, TIndexeMappingList>;
    FColumnMapping: TDictionary<string, TColumnMappingList>;
    FAssociationMapping: TDictionary<string, TAssociationMappingList>;
    FJoinColumnMapping: TDictionary<string, TJoinColumnMappingList>;
    constructor CreatePrivate;
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    class function GetInstance: TMappingExplorer;
    function GetMappingTable(AClass: TClass): TTableMapping; override;
    function GetMappingPrimaryKey(AClass: TClass): TPrimaryKeyMappingList; override;
    function GetMappingForeignKey(AClass: TClass): TForeignKeyMappingList; override;
    function GetMappingColumn(AClass: TClass): TColumnMappingList; override;
    function GetMappingAssociation(AClass: TClass): TAssociationMappingList; override;
    function GetMappingJoinColumn(AClass: TClass): TJoinColumnMappingList; override;
    function GetMappingIndexe(AClass: TClass): TIndexeMappingList; override;
    function GetColumnByName(AClass: TClass; AColumnName: string): TColumnMapping; override;
    property Repository: TMappingRepository read FRepositoryMapping;
  end;

implementation

{ TMappingExplorer }

function TMappingExplorer.GetColumnByName(AClass: TClass; AColumnName: string): TColumnMapping;
begin
  inherited;
  for Result in GetMappingColumn(AClass) do
    if SameText(Result.Name, AColumnName) then
      Exit;
  Result := nil;
end;

constructor TMappingExplorer.Create;
begin
   raise Exception.Create('Para usar o MappingEntity use o m�todo TMappingExplorerClass.GetInstance()');
end;

constructor TMappingExplorer.CreatePrivate;
begin
  inherited;
  FRepositoryMapping := TMappingRepository.Create(TRegisterClasse.GetAllEntityClass);
  FPopularMapping := TMappingPopular.Create(Self);

  FTableMapping       := TObjectDictionary<string, TTableMapping>.Create([doOwnsValues]);
  FPrimaryKeyMapping  := TObjectDictionary<string, TPrimaryKeyMappingList>.Create([doOwnsValues]);
  FForeingnKeyMapping := TObjectDictionary<string, TForeignKeyMappingList>.Create([doOwnsValues]);
  FColumnMapping      := TObjectDictionary<string, TColumnMappingList>.Create([doOwnsValues]);
  FAssociationMapping := TObjectDictionary<string, TAssociationMappingList>.Create([doOwnsValues]);
  FJoinColumnMapping  := TObjectDictionary<string, TJoinColumnMappingList>.Create([doOwnsValues]);
  FIndexeMapping      := TObjectDictionary<string, TIndexeMappingList>.Create([doOwnsValues]);
end;

destructor TMappingExplorer.Destroy;
begin
  FPopularMapping.Free;
  FTableMapping.Free;
  FPrimaryKeyMapping.Free;
  FForeingnKeyMapping.Free;
  FColumnMapping.Free;
  FAssociationMapping.Free;
  FJoinColumnMapping.Free;
  FIndexeMapping.Free;
  if Assigned(FRepositoryMapping) then
     FRepositoryMapping.Free;
  inherited;
end;

class function TMappingExplorer.GetInstance: TMappingExplorer;
begin
   if not Assigned(FInstance) then
      FInstance := TMappingExplorer.CreatePrivate;

   Result := FInstance;
end;

function TMappingExplorer.GetMappingPrimaryKey(AClass: TClass): TPrimaryKeyMappingList;
var
  oRttiType: TRttiType;
begin
  if FPrimaryKeyMapping.ContainsKey(AClass.ClassName) then
     Exit(FPrimaryKeyMapping[AClass.ClassName]);

  oRttiType := TRttiSingleton.GetInstance.GetRttiType(AClass);
  Result    := FPopularMapping.PopularPrimaryKey(oRttiType);
   /// Add List
  FPrimaryKeyMapping.Add(AClass.ClassName, Result);
end;

function TMappingExplorer.GetMappingColumn(AClass: TClass): TColumnMappingList;
var
  oRttiType: TRttiType;
begin
  if FColumnMapping.ContainsKey(AClass.ClassName) then
     Exit(FColumnMapping[AClass.ClassName]);

  oRttiType := TRttiSingleton.GetInstance.GetRttiType(AClass);
  Result    := FPopularMapping.PopularColumn(oRttiType);
   /// Add List
  FColumnMapping.Add(AClass.ClassName, Result);
end;

function TMappingExplorer.GetMappingForeignKey(AClass: TClass): TForeignKeyMappingList;
var
  oRttiType: TRttiType;
begin
  if FForeingnKeyMapping.ContainsKey(AClass.ClassName) then
     Exit(FForeingnKeyMapping[AClass.ClassName]);

  oRttiType := TRttiSingleton.GetInstance.GetRttiType(AClass);
  Result    := FPopularMapping.PopularForeignKey(oRttiType);
   /// Add List
  FForeingnKeyMapping.Add(AClass.ClassName, Result);
end;

function TMappingExplorer.GetMappingIndexe(AClass: TClass): TIndexeMappingList;
var
  oRttiType: TRttiType;
begin
  if FIndexeMapping.ContainsKey(AClass.ClassName) then
     Exit(FIndexeMapping[AClass.ClassName]);

  oRttiType := TRttiSingleton.GetInstance.GetRttiType(AClass);
  Result    := FPopularMapping.PopularIndexe(oRttiType);
   /// Add List
  FIndexeMapping.Add(AClass.ClassName, Result);
end;

function TMappingExplorer.GetMappingJoinColumn(AClass: TClass): TJoinColumnMappingList;
var
  oRttiType: TRttiType;
begin
  if FJoinColumnMapping.ContainsKey(AClass.ClassName) then
     Exit(FJoinColumnMapping[AClass.ClassName]);

  oRttiType := TRttiSingleton.GetInstance.GetRttiType(AClass);
  Result    := FPopularMapping.PopularJoinColumn(oRttiType);
   /// Add List
  FJoinColumnMapping.Add(AClass.ClassName, Result);
end;

function TMappingExplorer.GetMappingAssociation(AClass: TClass): TAssociationMappingList;
var
  oRttiType: TRttiType;
begin
  if FAssociationMapping.ContainsKey(AClass.ClassName) then
     Exit(FAssociationMapping[AClass.ClassName]);

  oRttiType := TRttiSingleton.GetInstance.GetRttiType(AClass);
  Result    := FPopularMapping.PopularAssociation(oRttiType);
   /// Add List
  FAssociationMapping.Add(AClass.ClassName, Result);
end;

function TMappingExplorer.GetMappingTable(AClass: TClass): TTableMapping;
var
  oRttiType: TRttiType;
begin
  if FTableMapping.ContainsKey(AClass.ClassName) then
     Exit(FTableMapping[AClass.ClassName]);

  oRttiType := TRttiSingleton.GetInstance.GetRttiType(AClass);
  Result    := FPopularMapping.PopularTable(oRttiType);
   /// Add List
  FTableMapping.Add(AClass.ClassName, Result);
end;

initialization

finalization
   if Assigned(TMappingExplorer.FInstance) then
      TMappingExplorer.FInstance.Free;

end.

