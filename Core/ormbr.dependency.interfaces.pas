{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.dependency.interfaces;

interface

uses
  Classes,
  ormbr.factory.interfaces,
  ormbr.bind.base;

type
  IContainerDataSet<T: class, constructor> = interface
    ['{55C7F31F-6D2C-4F1F-BF09-8DA8A6FF72D8}']
  {$REGION 'Property Getters & Setters'}
    function GetDatasetBase: TDatasetBase<T>;
  {$ENDREGION}
    property DataSet: TDatasetBase<T> read GetDatasetBase;
  end;

implementation

end.
