{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.database.mapping;

interface

uses
  Generics.Collections,
  ormbr.types.mapping;

type
  TmkTable = class;
  TmkCatalogMetadata = class;

  TMetaInfoKind = class abstract
  end;

  TmkTableField = class(TMetaInfoKind)
  strict private
    FTable: TmkTable;
    FName: string;
    FPosition: Integer;
    FDataType: Integer;
    FTypeName: string;
    FLength: Integer;
    FPrecision: Integer;
    FScale: Integer;
    FNotNull: Boolean;
    FAutoIncrement: boolean;
    FDefaultValue: string;
  public
    constructor Create(ATable: TmkTable);
    property Table: TmkTable read FTable;
    property Name: string read FName write FName;
    property Position: Integer read FPosition write FPosition;
    property DataType: Integer read FDataType write FDataType;
    property TypeName: string read FTypeName write FTypeName;
    property Length: Integer read FLength write FLength;
    property Precision: Integer read FPrecision write FPrecision;
    property Scale: Integer read FScale write FScale;
    property NotNull: Boolean read FNotNull write FNotNull;
    property AutoIncrement: boolean read FAutoIncrement write FAutoIncrement;
    property DefaultValue: string read FDefaultValue write FDefaultValue;
  end;

  TmkIndexeKey = class(TMetaInfoKind)
  strict private
    FTable: TmkTable;
    FName: string;
    FUnique: Boolean;
    FFields: TList<TmkTableField>;
  public
    constructor Create(ATable: TmkTable);
    destructor Destroy; override;
    property Table: TmkTable read FTable;
    property Name: string read FName write FName;
    property Unique: Boolean read FUnique write FUnique;
    property Fields: TList<TmkTableField> read FFields;
  end;

  TmkAutoIncrement = class(TMetaInfoKind)
  strict private
    FName: string;
    FInitialValue: Integer;
    FIncrement: Integer;
    FCatalog: TmkCatalogMetadata;
  public
    constructor Create(ADatabase: TmkCatalogMetadata);
    property Name: string read FName write FName;
    property InitialValue: Integer read FInitialValue write FInitialValue;
    property Increment: Integer read FIncrement write FIncrement;
    property Database: TmkCatalogMetadata read FCatalog;
  end;

  TmkForeignKey = class(TMetaInfoKind)
  strict private
    FName: string;
    FFromTable: TmkTable;
    FFromFields: TList<TmkTableField>;
    FToTable: TmkTable;
    FToFields: TList<TmkTableField>;
    FOnUpdate: TRuleAction;
    FOnDelete: TRuleAction;
  public
    constructor Create(AFromTable: TmkTable);
    destructor Destroy; override;
    property Name: string read FName write FName;
    property FromTable: TmkTable read FFromTable;
    property FromFields: TList<TmkTableField> read FFromFields write FFromFields;
    property ToTable: TmkTable read FToTable write FToTable;
    property ToFields: TList<TmkTableField> read FToFields write FToFields;
    property OnUpdate: TRuleAction read FOnUpdate write FOnUpdate;
    property OnDelete: TRuleAction read FOnDelete write FOnDelete;
  end;

  TmkPrimaryKey = class(TMetaInfoKind)
  strict private
    FTable: TmkTable;
    FName: string;
    FPrimaryKeyFields: TList<TmkTableField>;
  public
    constructor Create(ATable: TmkTable);
    destructor Destroy; override;
    property Name: string read FName write FName;
    property PrimaryKeyFields: TList<TmkTableField> read FPrimaryKeyFields;
  end;

  TmkTable = class(TMetaInfoKind)
  strict private
    FCatalog: TmkCatalogMetadata;
    FName: string;
    FPrimaryKey: TmkPrimaryKey;
    FFields: TList<TmkTableField>;
    FIndexeKeys: TList<TmkIndexeKey>;
    FForeignKeys: TList<TmkForeignKey>;
  public
    constructor Create(ADatabase: TmkCatalogMetadata);
    destructor Destroy; override;
    property Database: TmkCatalogMetadata read FCatalog;
    property Name: string read FName write FName;
    property Fields: TList<TmkTableField> read FFields;
    property PrimaryKey: TmkPrimaryKey read FPrimaryKey;
    property IndexeKeys: TList<TmkIndexeKey> read FIndexeKeys;
    property ForeignKeys: TList<TmkForeignKey> read FForeignKeys;
  end;

  TmkCatalogMetadata = class(TMetaInfoKind)
  strict private
    FName: string;
    FSchema: string;
    FTables: TList<TmkTable>;
    FAutoIncrements: TList<TmkAutoIncrement>;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    property Name: string read FName write FName;
    property Schema: string read FSchema write FSchema;
    property Tables: TList<TmkTable> read FTables;
    property AutoIncrements: TList<TmkAutoIncrement> read FAutoIncrements;
  end;

implementation

{ TmkTable }

constructor TmkTable.Create(ADatabase: TmkCatalogMetadata);
begin
  FFields := TObjectList<TmkTableField>.Create(True);
  FIndexeKeys := TObjectList<TmkIndexeKey>.Create(True);
  FForeignKeys := TObjectList<TmkForeignKey>.Create(True);
  FPrimaryKey := TmkPrimaryKey.Create(Self);
  FCatalog := ADatabase;
end;

destructor TmkTable.Destroy;
begin
  FPrimaryKey.Free;
  FFields.Free;
  FIndexeKeys.Free;
  FForeignKeys.Free;
  inherited;
end;

{ TmkForeignKey }

constructor TmkForeignKey.Create(AFromTable: TmkTable);
begin
  FFromTable := AFromTable;
  FFromFields := TList<TmkTableField>.Create;
  FToFields := TList<TmkTableField>.Create;
end;

destructor TmkForeignKey.Destroy;
begin
  FFromFields.Free;
  FToFields.Free;
  inherited;
end;

{ TmkIndexeKey }

constructor TmkIndexeKey.Create(ATable: TmkTable);
begin
  FFields := TList<TmkTableField>.Create;
  FTable := ATable;
end;

destructor TmkIndexeKey.Destroy;
begin
  FFields.Free;
  inherited;
end;

{ TmkCatalogMetadata }

procedure TmkCatalogMetadata.Clear;
begin
  Tables.Clear;
  AutoIncrements.Clear;
end;

constructor TmkCatalogMetadata.Create;
begin
  FTables := TObjectList<TmkTable>.Create(True);
  FAutoIncrements := TObjectList<TmkAutoIncrement>.Create(True);
end;

destructor TmkCatalogMetadata.Destroy;
begin
  FAutoIncrements.Free;
  FTables.Free;
  inherited;
end;

{ TmkTableField }

constructor TmkTableField.Create(ATable: TmkTable);
begin
  FTable := ATable;
end;

{ TmkSequence }

constructor TmkAutoIncrement.Create(ADatabase: TmkCatalogMetadata);
begin
  FCatalog := ADatabase;
end;

{ TmkPrimaryKey }

constructor TmkPrimaryKey.Create(ATable: TmkTable);
begin
  FTable := ATable;
  FPrimaryKeyFields := TList<TmkTableField>.Create;
end;

destructor TmkPrimaryKey.Destroy;
begin
  FPrimaryKeyFields.Free;
  inherited;
end;

end.
