{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.metadata.db.factory;

interface

uses
  SysUtils,
  Rtti,
  Generics.Collections,
  ormbr.metadata.register,
  ormbr.factory.interfaces,
  ormbr.metadata.interfaces,
  ormbr.database.mapping;

type
  TMetadataDBAbstract = class abstract
  protected
    FConnection: IDBConnection;
    FDatabaseMetadata: IDatabaseMetadata;
    FCatalogMetadata: TmkCatalogMetadata;
    procedure ExtractCatalogs; virtual; abstract;
    procedure ExtractSchemas; virtual; abstract;
    procedure ExtractTables; virtual; abstract;
  public
    procedure ExtractMetadata; virtual; abstract;
  end;

  TMetadataDBFactory = class(TMetadataDBAbstract)
  protected
    procedure ExtractCatalogs; override;
    procedure ExtractSchemas; override;
    procedure ExtractTables; override;
  public
    constructor Create(AConnection: IDBConnection; ACatalogMetadata: TmkCatalogMetadata); virtual;
    procedure ExtractMetadata; override;
  end;

implementation

{ TMetadataFactory }

constructor TMetadataDBFactory.Create(AConnection: IDBConnection; ACatalogMetadata: TmkCatalogMetadata);
begin
  FConnection := AConnection;
  FCatalogMetadata := ACatalogMetadata;
  FDatabaseMetadata := TMetadataRegister.GetMetadata(AConnection.GetDriverName);
  FDatabaseMetadata.Connection := AConnection;
  FDatabaseMetadata.CatalogMetadata := FCatalogMetadata;
end;

procedure TMetadataDBFactory.ExtractMetadata;
begin
  /// <summary>
  /// Extrair catalogos do DB
  /// </summary>
  ExtractCatalogs;
end;

procedure TMetadataDBFactory.ExtractCatalogs;
begin
  FDatabaseMetadata.GetCatalogs;
  /// <summary>
  /// Extrair Schemas do DB
  /// </summary>
  ExtractSchemas;
end;

procedure TMetadataDBFactory.ExtractSchemas;
begin
  FDatabaseMetadata.GetSchemas;
  /// <summary>
  ///
  /// </summary>
  ExtractTables;
end;

procedure TMetadataDBFactory.ExtractTables;
begin
  FDatabaseMetadata.GetTables;
end;

end.
