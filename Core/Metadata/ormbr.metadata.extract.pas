{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.metadata.extract;

interface

uses
  ormbr.factory.interfaces,
  ormbr.metadata.interfaces,
  ormbr.database.mapping,
  ormbr.mapping.repository;

type
  TCatalogMetadataAbstract = class(TInterfacedObject, IDatabaseMetadata)
  private
    function GetConnection: IDBConnection;
    procedure SetConnection(const Value: IDBConnection);
    function GetCatalogMetadata: TmkCatalogMetadata;
    procedure SetCatalogMetadata(const Value: TmkCatalogMetadata);
  protected
    FSQL: string;
    FConnection: IDBConnection;
    FCatalogMetadata: TmkCatalogMetadata;
    procedure GetCatalogs; virtual; abstract;
    procedure GetSchemas; virtual; abstract;
    procedure GetTables; virtual; abstract;
    procedure GetColumns(ATable: TmkTable); virtual; abstract;
    procedure GetPrimaryKey(ATable: TmkTable); virtual; abstract;
    procedure GetIndexeKeys(ATable: TmkTable); virtual; abstract;
    procedure GetForeignKeys(ATable: TmkTable); virtual; abstract;
    procedure GetTriggers(ATable: TmkTable); virtual; abstract;
    procedure GetSequences; virtual; abstract;
    procedure GetProcedures; virtual; abstract;
    procedure GetFunctions; virtual; abstract;
    procedure GetViews; virtual; abstract;
  public
    property Connection: IDBConnection read GetConnection write SetConnection;
    property CatalogMetadata: TmkCatalogMetadata read GetCatalogMetadata write SetCatalogMetadata;
  end;

  TModelMetadataAbstract = class(TInterfacedObject, IModelMetadata)
  protected
    FCatalogMetadata: TmkCatalogMetadata;
    procedure GetCatalogs; virtual; abstract;
    procedure GetSchemas; virtual; abstract;
    procedure GetTables; virtual; abstract;
    procedure GetColumns(ATableName: string); virtual; abstract;
    procedure GetPrimaryKey; virtual; abstract;
    procedure GetIndexeKeys; virtual; abstract;
    procedure GetForeignKeys; virtual; abstract;
    procedure GetSequences; virtual; abstract;
    procedure GetProcedures; virtual; abstract;
    procedure GetFunctions; virtual; abstract;
    procedure GetViews; virtual; abstract;
    procedure GetTriggers; virtual; abstract;
    procedure GetModelMetadata; virtual; abstract;
  public
    constructor Create(ACatalogMetadata: TmkCatalogMetadata);
  end;

implementation

{ TCatalogMetadataAbstract }

function TCatalogMetadataAbstract.GetCatalogMetadata: TmkCatalogMetadata;
begin
  Result := FCatalogMetadata;
end;

function TCatalogMetadataAbstract.GetConnection: IDBConnection;
begin
  Result := FConnection;
end;

procedure TCatalogMetadataAbstract.SetCatalogMetadata(const Value: TmkCatalogMetadata);
begin
  FCatalogMetadata := Value;
end;

procedure TCatalogMetadataAbstract.SetConnection(const Value: IDBConnection);
begin
  FConnection := Value;
end;

{ TModelMetadataAbstract }

constructor TModelMetadataAbstract.Create(ACatalogMetadata: TmkCatalogMetadata);
begin
  FCatalogMetadata := ACatalogMetadata;
end;

end.
