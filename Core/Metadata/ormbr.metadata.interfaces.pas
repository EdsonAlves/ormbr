{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.metadata.interfaces;

interface

uses
  ormbr.factory.interfaces,
  ormbr.database.mapping;

type
  IDatabaseMetadata = interface
    ['{3B007175-E205-4051-88F3-8FD18F5D3C8F}']
  {$REGION 'Property Getters & Setters'}
    function GetConnection: IDBConnection;
    procedure SetConnection(const Value: IDBConnection);
    function GetCatalogMetadata: TmkCatalogMetadata;
    procedure SetCatalogMetadata(const Value: TmkCatalogMetadata);
  {$ENDREGION}
    procedure GetCatalogs;
    procedure GetSchemas;
    procedure GetTables;
    property Connection: IDBConnection read GetConnection write SetConnection;
    property CatalogMetadata: TmkCatalogMetadata read GetCatalogMetadata write SetCatalogMetadata;
  end;

  IModelMetadata = interface
    ['{AEDA745A-5E7F-4C03-A7B0-ED3176409A45}']
    procedure GetModelMetadata;
  end;

implementation

end.
