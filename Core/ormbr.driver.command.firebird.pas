{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.driver.command.firebird;

interface

uses
  SysUtils,
  StrUtils,
  Rtti,
  ormbr.driver.command,
  ormbr.mapping.classes,
  ormbr.criteria.interfaces,
  ormbr.mapping.explorer,
  ormbr.factory.interfaces,
  ormbr.driver.register,
  GpSQLBuilder;

type
  /// <summary>
  /// Classe de conex�o concreta com dbExpress
  /// </summary>
  TDriverFirebird = class(TDriverCommand)
  protected
    function GetBuildSQLSelect(ACriteria: IGpSQLBuilder): string; override;
  public
    constructor Create; override;
    destructor Destroy; override;
    function BuildSQLSelectAll(AClass: TClass; APageSize: Integer; AID: TValue): string; override;
    function BuildSQLSequence(AClass: TClass): string; override;
  end;

implementation

{ TDriverFirebird }

function TDriverFirebird.BuildSQLSequence(AClass: TClass): string;
begin

end;

constructor TDriverFirebird.Create;
begin
  inherited;

end;

destructor TDriverFirebird.Destroy;
begin

  inherited;
end;

function TDriverFirebird.GetBuildSQLSelect(ACriteria: IGpSQLBuilder): string;
begin
  inherited;
  ACriteria.AST.Select.Columns.Columns[0].Name := 'FIRST %u SKIP %u';
  Result := ACriteria.AsString;
end;

function TDriverFirebird.BuildSQLSelectAll(AClass: TClass; APageSize: Integer; AID: TValue): string;
var
  oCriteria: IGpSQLBuilder;
begin
  oCriteria := GetCriteriaSelect(AClass, AID);
  if APageSize > -1 then
  begin
     Result := GetBuildSQLSelect(oCriteria);
     Result := ReplaceStr(Result, '%u,', '%u');
  end
  else
     Result   := oCriteria.AsString;
end;

initialization
  TDriverRegister.RegisterDriver(dnFirebird, TDriverFirebird.Create);

end.
