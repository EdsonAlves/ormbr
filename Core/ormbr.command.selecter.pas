{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.command.selecter;

interface

uses
  SysUtils,
  Rtti,
  GpSQLBuilder,
  ormbr.command.base,
  ormbr.factory.interfaces;

type
  TCommandSelecter = class(TCommandBase)
  private
    FPageSize: Integer;
    FPageNext: Integer;
    FSelectSQL: string;
  public
    constructor Create(AConnection: IDBConnection; ADriverName: TDriverName); override;
    procedure SetPageSize(APageSize: Integer);
    procedure GenerateSelect(ASQL: IGpSQLBuilder);
    procedure GenerateSelectAll(AClass: TClass);
    procedure GenerateSelectID(AClass: TClass; AID: TValue);
    procedure GenerateNextPacket;
    function ExecuteSelect: IDBResultSet;
  end;

implementation

{ TCommandSelecter }

procedure TCommandSelecter.GenerateNextPacket;
begin
  inherited;
  FPageNext := FPageNext + FPageSize;
  if FPageSize > -1 then
     FSQL := Format(FSelectSQL, [FPageSize, FPageNext]);
end;

procedure TCommandSelecter.SetPageSize(APageSize: Integer);
begin
  inherited;
  FPageSize := APageSize;
end;

procedure TCommandSelecter.GenerateSelect(ASQL: IGpSQLBuilder);
begin
  inherited;
  FPageNext := 0;
  FSelectSQL := FDriverCommand.BuildSQLSelect(ASQL, FPageSize);
  if FPageSize > -1 then
     FSQL := Format(FSelectSQL, [FPageSize, FPageNext])
  else
     FSQL := FSelectSQL;
end;

procedure TCommandSelecter.GenerateSelectAll(AClass: TClass);
begin
  inherited;
  FPageNext := 0;
  FSelectSQL := FDriverCommand.BuildSQLSelectAll(AClass, FPageSize, -1);
  if FPageSize > -1 then
     FSQL := Format(FSelectSQL, [FPageSize, FPageNext])
  else
     FSQL := FSelectSQL;
end;

procedure TCommandSelecter.GenerateSelectID(AClass: TClass; AID: TValue);
begin
  inherited;
  FPageNext := 0;
  FSelectSQL := FDriverCommand.BuildSQLSelectAll(AClass, FPageSize, AID);
  if FPageSize > -1 then
     FSQL := Format(FSelectSQL, [FPageSize, FPageNext])
  else
     FSQL := FSelectSQL;
end;

constructor TCommandSelecter.Create(AConnection: IDBConnection; ADriverName: TDriverName);
begin
  inherited Create(AConnection, ADriverName);
  FSelectSQL := '';
  FSQL := '';
  FPageSize := -1;
  FPageNext := 0;
end;

function TCommandSelecter.ExecuteSelect: IDBResultSet;
var
  oSQLQuery: IDBQuery;
begin
  inherited;
  oSQLQuery := FConnection.CreateQuery;
  try
    oSQLQuery.CommandText := FSQL;
    Exit(oSQLQuery.ExecuteQuery);
  except
    raise
  end;
end;

end.
