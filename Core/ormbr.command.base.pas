{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.command.base;

interface

uses
  DB,
  Rtti,
  GpSQLBuilder,
  ormbr.command.interfaces,
  ormbr.factory.interfaces,
  ormbr.driver.register,
  ormbr.driver.command;

type
  TCommandBase = class abstract
  protected
    FConnection: IDBConnection;
    FDriverCommand: TDriverCommand;
    FSQL: string;
    procedure ExecuteDirect; virtual;
  public
    constructor Create(AConnection: IDBConnection; ADriverName: TDriverName); virtual;
  end;

implementation

{ TCommandBase }

constructor TCommandBase.Create(AConnection: IDBConnection; ADriverName: TDriverName);
begin
  /// <summary>
  /// Driver de Conex�o
  /// </summary>
  FConnection := AConnection;
  /// <summary>
  /// Driver de Comando
  /// </summary>
  FDriverCommand := TDriverRegister.GetDriver(ADriverName);
end;

procedure TCommandBase.ExecuteDirect;
var
  oSQLQuery: IDBQuery;
begin
   oSQLQuery := FConnection.CreateQuery;
   try
     oSQLQuery.CommandText := FSQL;
     oSQLQuery.ExecuteDirect;
   except
     raise
   end;
end;

end.
