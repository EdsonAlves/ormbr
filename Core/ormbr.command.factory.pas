{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.command.factory;

interface

uses
  DB,
  Rtti,
  Generics.Collections,
  GpSQLBuilder,
  ormbr.command.interfaces,
  ormbr.factory.interfaces,
  ormbr.driver.command,
  ormbr.command.selecter,
  ormbr.command.inserter,
  ormbr.command.deleter,
  ormbr.command.updater;

type
  TSQLCommandFactory = class abstract(TInterfacedObject, IBuildSQLCommand)
  protected
    FCommandSelecter: TCommandSelecter;
    FCommandInserter: TCommandInserter;
    FCommandUpdater: TCommandUpdater;
    FCommandDeleter: TCommandDeleter;
  public
    constructor Create(const ADBConnection: IDBConnection; const ADriverName: TDriverName); virtual;
    destructor Destroy; override;
    function BuildSQLSelectAll(AClass: TClass; APageSize: Integer): IDBResultSet; virtual;
    function BuildSQLSelectID(AClass: TClass; AID:TValue; APageSize: Integer): IDBResultSet; virtual;
    function BuildSQLSelect(ASQL: IGpSQLBuilder; APageSize: Integer): IDBResultSet; virtual;
    function BuildSQLNextPacket: IDBResultSet; virtual;
    function BuildSQLSequence(AClass: TClass): IDBResultSet; virtual;
    procedure BuildSQLUpdate(AObject: TObject); overload; virtual;
    procedure BuildSQLUpdate(AObject: TObject; AModifiedFields: TDictionary<string, TField>); overload; virtual;
    procedure BuildSQLInsert(AObject: TObject); virtual;
    procedure BuildSQLDelete(AObject: TObject); virtual;
  end;

implementation

uses
  ormbr.driver.command.firebird,
  ormbr.driver.command.mssql,
  ormbr.driver.command.mysql,
  ormbr.driver.command.sqlite;

{ TSQLCommandFactory }

constructor TSQLCommandFactory.Create(const ADBConnection: IDBConnection; const ADriverName: TDriverName);
begin
  FCommandSelecter := TCommandSelecter.Create(ADBConnection, ADriverName);
  FCommandInserter := TCommandInserter.Create(ADBConnection, ADriverName);
  FCommandUpdater  := TCommandUpdater.Create(ADBConnection, ADriverName);
  FCommandDeleter  := TCommandDeleter.Create(ADBConnection, ADriverName);
end;

destructor TSQLCommandFactory.Destroy;
begin
  FCommandSelecter.Free;
  FCommandDeleter.Free;
  FCommandInserter.Free;
  FCommandUpdater.Free;
  inherited;
end;

procedure TSQLCommandFactory.BuildSQLDelete(AObject: TObject);
begin
  FCommandDeleter.GenerateDelete(AObject);
  FCommandDeleter.ExecuteDelete;
end;

procedure TSQLCommandFactory.BuildSQLInsert(AObject: TObject);
begin
  FCommandInserter.GenerateInsert(AObject);
  FCommandInserter.ExecuteInsert;
end;

function TSQLCommandFactory.BuildSQLSelect(ASQL: IGpSQLBuilder; APageSize: Integer): IDBResultSet;
begin
  FCommandSelecter.SetPageSize(APageSize);
  FCommandSelecter.GenerateSelect(ASQL);
  Result := FCommandSelecter.ExecuteSelect;
end;

function TSQLCommandFactory.BuildSQLSelectAll(AClass: TClass; APageSize: Integer): IDBResultSet;
begin
  FCommandSelecter.SetPageSize(APageSize);
  FCommandSelecter.GenerateSelectAll(AClass);
  Result := FCommandSelecter.ExecuteSelect;
end;

function TSQLCommandFactory.BuildSQLSelectID(AClass: TClass; AID: TValue; APageSize: Integer): IDBResultSet;
begin
  FCommandSelecter.SetPageSize(APageSize);
  FCommandSelecter.GenerateSelectID(AClass, AID);
  Result := FCommandSelecter.ExecuteSelect;
end;

function TSQLCommandFactory.BuildSQLSequence(AClass: TClass): IDBResultSet;
begin

end;

function TSQLCommandFactory.BuildSQLNextPacket: IDBResultSet;
begin
  FCommandSelecter.GenerateNextPacket;
  Result := FCommandSelecter.ExecuteSelect;
end;

procedure TSQLCommandFactory.BuildSQLUpdate(AObject: TObject);
begin
  FcommandUpdater.GenerateUpdate(AObject);
  FcommandUpdater.ExecuteUpdate;
end;

procedure TSQLCommandFactory.BuildSQLUpdate(AObject: TObject; AModifiedFields: TDictionary<string, TField>);
begin
  FcommandUpdater.GenerateUpdate(AObject, AModifiedFields);
  FcommandUpdater.ExecuteUpdate;
end;

end.
