{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.mapping.register;

interface

uses
  Rtti,
  Generics.Collections,
  ormbr.mapping.attributes;

type
  TRegisterClasse = class
  strict private
    FRegistered: TList<TClass>;
//    function FRegistered;//GetClass: TList<TClass>;
  public
    class function GetAllEntityClass: TArray<TClass>;
  public
    constructor Create;
    destructor Destroy; override;
    procedure RegisterClass(AClass: TClass); overload;
    procedure RegisterClass(AClass: TList<TClass>); overload;
    procedure RegisterClass(AClass: TArray<TClass>); overload;
    procedure UnregisterClass(AClass: TClass);
    procedure Clear;
    function IsEmpty: boolean;
    property List: TList<TClass> read FRegistered;//GetClass;
  end;

implementation

uses
  SysUtils;

{ TMappedClasses }

procedure TRegisterClasse.RegisterClass(AClass: TClass);
begin
  if not FRegistered.Contains(AClass) then
     FRegistered.Add(AClass);
end;

procedure TRegisterClasse.RegisterClass(AClass: TArray<TClass>);
var
  oClass: TClass;
begin
  for oClass in AClass do
     RegisterClass(oClass);
end;

procedure TRegisterClasse.RegisterClass(AClass: TList<TClass>);
var
  oClass: TClass;
begin
  for oClass in AClass do
     RegisterClass(oClass);
end;

procedure TRegisterClasse.UnregisterClass(AClass: TClass);
begin
  FRegistered.Remove(AClass);
end;

procedure TRegisterClasse.Clear;
begin
  FRegistered.Clear;
end;

constructor TRegisterClasse.Create;
begin
  FRegistered := TList<TClass>.Create;
end;

destructor TRegisterClasse.Destroy;
begin
  FRegistered.Free;
  inherited;
end;

//function TRegisterClasse.FRegistered;//GetClass: TList<TClass>;
//begin
//  Result := FRegistered;
//end;

class function TRegisterClasse.GetAllEntityClass: TArray<TClass>;
var
  oContext: TRttiContext;
  oAllTypes: TArray<TRttiType>;
  oClassType: TRttiType;
  oClassAttr: TCustomAttribute;
  oEntityClass: TList<TClass>;
  iFor: Integer;
begin
   oContext := TRttiContext.Create;
   oEntityClass := TList<TClass>.Create;
   try
     oAllTypes := oContext.GetTypes;
     for oClassType in oAllTypes do
     begin
        if oClassType.IsInstance then
        begin
           for oClassAttr in oClassType.GetAttributes do
           begin
              if oClassAttr is Entity then // Entity
                 oEntityClass.Add(oClassType.AsInstance.MetaclassType);
           end;
        end;
     end;
     SetLength(Result, oEntityClass.Count);
     for iFor := 0 to oEntityClass.Count -1 do
        Result[iFor] := oEntityClass[iFor];
   finally
     oContext.Free;
     oEntityClass.Free;
   end;
end;

function TRegisterClasse.IsEmpty: boolean;
begin
  Result := FRegistered.Count = 0;
end;

end.


