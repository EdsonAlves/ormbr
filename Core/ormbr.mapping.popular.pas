{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.mapping.popular;

interface

uses
  DB,
  Classes,
  Rtti,
  TypInfo,
  Generics.Collections,
  ormbr.mapping.rttiutils,
  ormbr.mapping.attributes,
  ormbr.mapping.classes,
  ormbr.mapping.helper,
  ormbr.mapping.explorerstrategy;

type
  TMappingPopular = class
  private
    FMappingExplorerStrategy: TMappingExplorerStrategy;
  public
    constructor Create(AMappingExplorerStrategy: TMappingExplorerStrategy);
    function PopularTable(ARttiType: TRttiType): TTableMapping;
    function PopularPrimaryKey(ARttiType: TRttiType): TPrimaryKeyMappingList;
    function PopularForeignKey(ARttiType: TRttiType): TForeignKeyMappingList;
    function PopularIndexe(ARttiType: TRttiType): TIndexeMappingList;
    function PopularColumn(ARttiType: TRttiType): TColumnMappingList;
    function PopularAssociation(ARttiType: TRttiType): TAssociationMappingList;
    function PopularJoinColumn(ARttiType: TRttiType): TJoinColumnMappingList;
  end;

implementation

{ TMappingPopular }

constructor TMappingPopular.Create(AMappingExplorerStrategy: TMappingExplorerStrategy);
begin
  FMappingExplorerStrategy := AMappingExplorerStrategy;
end;

function TMappingPopular.PopularColumn(ARttiType: TRttiType): TColumnMappingList;
var
  oProperty: TRttiProperty;
  oAttrib: TCustomAttribute;
begin
  Result := nil;
  for oProperty in ARttiType.GetProperties do
  begin
     for oAttrib in oProperty.GetAttributes do
     begin
        if oProperty.IsJoinColumn then
          Continue;

        if oAttrib is Column then // Column
        begin
           if Result = nil then
              Result := TColumnMappingList.Create;

           Result.Add(TColumnMapping.Create);
           with Result.Last do
           begin
              Name := Column(oAttrib).ColumnName;
              FieldType := Column(oAttrib).FieldType;
              Precision := Column(oAttrib).Precision;
              Size := Column(oAttrib).Size;
              Scale := 0;
           end;
        end;
     end;
  end;
end;

function TMappingPopular.PopularForeignKey(ARttiType: TRttiType): TForeignKeyMappingList;
var
  oProperty: TRttiProperty;
  oAttrib: TCustomAttribute;
begin
  Result := nil;
  for oProperty in ARttiType.GetProperties do
  begin
     for oAttrib in oProperty.GetAttributes do
     begin
        if oAttrib is ForeignKey then // ForeignKey
        begin
           if Result = nil then
              Result := TForeignKeyMappingList.Create;
           Result.Add(TForeignKeyMapping.Create(ForeignKey(oAttrib).ReferenceTableName,
                                                ForeignKey(oAttrib).RuleDelete,
                                                ForeignKey(oAttrib).RuleUpdate,
                                                ForeignKey(oAttrib).Description));
        end;
     end;
  end;
end;

function TMappingPopular.PopularIndexe(ARttiType: TRttiType): TIndexeMappingList;
var
  oProperty: TRttiProperty;
  oAttrib: TCustomAttribute;
begin
  Result := nil;
  for oProperty in ARttiType.GetProperties do
  begin
     for oAttrib in oProperty.GetAttributes do
     begin
        if oAttrib is Indexe then // Indexe
        begin
           if Result = nil then
              Result := TIndexeMappingList.Create;
           Result.Add(TIndexeMapping.Create(Indexe(oAttrib).Name,
                                            Indexe(oAttrib).Columns,
                                            Indexe(oAttrib).SortingOrder,
                                            Indexe(oAttrib).Unique,
                                            Indexe(oAttrib).Description));
        end;
     end;
  end;
end;

function TMappingPopular.PopularJoinColumn(ARttiType: TRttiType): TJoinColumnMappingList;
var
  oProperty: TRttiProperty;
  oAttrib: TCustomAttribute;
begin
  Result := nil;
  for oProperty in ARttiType.GetProperties do
  begin
    for oAttrib in oProperty.GetAttributes do
    begin
      if oAttrib is JoinColumn then // JoinColumn
      begin
        if Length(JoinColumn(oAttrib).ReferencedTableName) > 0 then
        begin
          if Result = nil then
             Result := TJoinColumnMappingList.Create;
          Result.Add(TJoinColumnMapping.Create(JoinColumn(oAttrib).ColumnName,
                                               JoinColumn(oAttrib).ReferencedTableName,
                                               JoinColumn(oAttrib).ReferencedColumnName,
                                               JoinColumn(oAttrib).Join));
        end;
      end;
    end;
  end;
end;

function TMappingPopular.PopularPrimaryKey(ARttiType: TRttiType): TPrimaryKeyMappingList;
var
  oProperty: TRttiProperty;
  oAttrib: TCustomAttribute;
begin
  Result := nil;
  for oProperty in ARttiType.GetProperties do
  begin
     for oAttrib in oProperty.GetAttributes do
     begin
        if oAttrib is PrimaryKey then // PrimaryKey
        begin
           if Result = nil then
              Result := TPrimaryKeyMappingList.Create;
           Result.Add(TPrimaryKeyMapping.Create(oProperty.Name,
                                                PrimaryKey(oAttrib).SortingOrder,
                                                PrimaryKey(oAttrib).Description));
        end;
     end;
  end;
end;

function TMappingPopular.PopularAssociation(ARttiType: TRttiType): TAssociationMappingList;
var
  oRttiType: TRttiType;
  oProperty: TRttiProperty;
  oAttrib: TCustomAttribute;
  oColumns: array of string;
  iFor: Integer;
begin
  Result := nil;
  for oProperty in ARttiType.GetProperties do
  begin
     for oAttrib in oProperty.GetAttributes do
     begin
        if oAttrib is Association then // Association
        begin
           oColumns := [];
           if Length(Association(oAttrib).ReferencedColumnName) > 0 then
           begin
              if Result = nil then
                 Result := TAssociationMappingList.Create;

              if Length(Association(oAttrib).Columns) > 0 then
              begin
                SetLength(oColumns, Length(Association(oAttrib).Columns));
                for iFor := 0 to High(Association(oAttrib).Columns) do
                  oColumns[iFor] := Association(oAttrib).Columns[iFor];
              end;
              oRttiType := ARttiType.GetProperty(oProperty.Name).PropertyType;
              oRttiType := TRttiSingleton.GetInstance.GetListType(oRttiType);
              if oRttiType <> nil then
                 Result.Add(TAssociationMapping.Create(Association(oAttrib).Multiplicity,
                                                       Association(oAttrib).ColumnName,
                                                       Association(oAttrib).ReferencedColumnName,
                                                       oRttiType.AsInstance.MetaclassType.ClassName,
                                                       oColumns))
              else
                 Result.Add(TAssociationMapping.Create(Association(oAttrib).Multiplicity,
                                                       Association(oAttrib).ColumnName,
                                                       Association(oAttrib).ReferencedColumnName,
                                                       oProperty.PropertyType.Name,
                                                       oColumns));
           end;
        end;
     end;
  end;
end;

function TMappingPopular.PopularTable(ARttiType: TRttiType): TTableMapping;
var
  oAttrib: TCustomAttribute;
begin
  Result := nil;
  for oAttrib in ARttiType.GetAttributes do
  begin
     if oAttrib is Table then // Table
     begin
       Result := TTableMapping.Create;
       Result.Name := Table(oAttrib).Name;
       Result.Schema := 'Model';
       Result.Description := Table(oAttrib).Description;
     end;
  end;
end;

end.

