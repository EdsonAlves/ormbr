{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.driver.command.mssql;

interface

uses
  SysUtils,
  Rtti,
  ormbr.driver.command,
  ormbr.mapping.classes,
  ormbr.factory.interfaces,
  ormbr.driver.register,
  GpSQLBuilder;

type
  /// <summary>
  /// Classe de conex�o concreta com dbExpress
  /// </summary>
  TDriverMSSql = class(TDriverCommand)
  protected
    function GetBuildSQLSelect(ACriteria: IGpSQLBuilder): string; override;
  public
    constructor Create; override;
    destructor Destroy; override;
    function BuildSQLSelectAll(AClass: TClass; APageSize: Integer; AID: TValue): string; override;
    function BuildSQLSequence(AClass: TClass): string; override;
  end;

implementation

{ TDriverMSSql }

function TDriverMSSql.BuildSQLSequence(AClass: TClass): string;
begin

end;

constructor TDriverMSSql.Create;
begin
  inherited;

end;

destructor TDriverMSSql.Destroy;
begin

  inherited;
end;

function TDriverMSSql.GetBuildSQLSelect(ACriteria: IGpSQLBuilder): string;
var
  sTable: string;
begin
   inherited;
   sTable := ACriteria.AST.Select.TableNames.Columns[0].Name;
   ACriteria.SelectSection(secSelect);
   ACriteria.Column('ROW_NUMBER() OVER(ORDER BY CURRENT_TIMESTAMP) AS ROWNUMBER');
   ACriteria.AST.Select.TableNames.Clear;
   ACriteria.From(sTable + ')').&As(sTable);
   ACriteria.SelectSection(secWhere);
   ACriteria.Where('(ROWNUMBER > %u) AND (ROWNUMBER <= %u)');
   ACriteria.SelectSection(secOrderBy);
   Result := 'SELECT * FROM (' + ACriteria.AsString;
end;

function TDriverMSSql.BuildSQLSelectAll(AClass: TClass; APageSize: Integer; AID: TValue): string;
var
  oCriteria: IGpSQLBuilder;
begin
   oCriteria := GetCriteriaSelect(AClass, AID);
   if APageSize > -1 then
      Result := GetBuildSQLSelect(oCriteria)
   else
      Result := oCriteria.AsString;
end;

initialization
  TDriverRegister.RegisterDriver(dnMSSQL, TDriverMSSql.Create);

end.
