{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.mapping.repository;

interface

uses
  SysUtils,
  Rtti,
  Generics.Collections,
  ormbr.mapping.exceptions;

type
  TMappingRepository = class
  private
    FRepository: TDictionary<TClass, TList<TClass>>;
    function FindClass(AClass: TClass): TList<TClass>;
    function GetList: TEnumerable<TClass>;
  public
    constructor Create(AClass: TArray<TClass>);
    destructor Destroy; override;
    function GetClass(AClass: TClass): TEnumerable<TClass>;
    function FindClassByName(ClassName: string): TClass;
    property List: TEnumerable<TClass> read GetList;
  end;

implementation

{ TMappingRepository }

constructor TMappingRepository.Create(AClass: TArray<TClass>);
var
  oClass: TClass;
begin
  FRepository := TObjectDictionary<TClass, TList<TClass>>.Create([doOwnsValues]);
  if AClass <> nil then
    for oClass in AClass do
      if not FRepository.ContainsKey(oClass) then
        FRepository.Add(oClass, TList<TClass>.Create);

  for oClass in Self.List do
    if FRepository.ContainsKey(oClass.ClassParent) then
      FRepository[oClass.ClassParent].Add(oClass);
end;

destructor TMappingRepository.Destroy;
begin
  FRepository.Free;
  inherited;
end;

function TMappingRepository.FindClassByName(ClassName: string): TClass;
var
  oClass: TClass;
begin
  for oClass in List do
     if SameText(oClass.ClassName, ClassName) then
        Exit(oClass);
  Result := nil;
end;

function TMappingRepository.FindClass(AClass: TClass): TList<TClass>;
var
  oClass: TClass;
  oListClass: TList<TClass>;
begin
  Result := TList<TClass>.Create;
  Result.AddRange(GetClass(AClass));

  for oClass in GetClass(AClass) do
  begin
    oListClass := FindClass(oClass);
    try
      Result.AddRange(oListClass);
    finally
      oListClass.Free;
    end;
  end;
end;

function TMappingRepository.GetList: TEnumerable<TClass>;
begin
  Result := FRepository.Keys;
end;

function TMappingRepository.GetClass(
  AClass: TClass): TEnumerable<TClass>;
begin
  if not FRepository.ContainsKey(AClass) then
     EClassNotRegistered.Create(AClass);

  Result := FRepository[AClass];
end;

end.

