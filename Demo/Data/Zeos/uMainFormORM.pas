unit uMainFormORM;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Variants,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  DB,
  Grids,
  DBGrids,
  StdCtrls,
  Mask,
  DBClient,
  DBCtrls,
  ExtCtrls,
  /// orm factory
  ormbr.factory.interfaces,
  /// orm injection dependency
  ormbr.dependency.interfaces,
  ormbr.dependency.injection.clientdataset,
  ormbr.dependency.injection.fdmemtable,
  ormbr.factory.zeos,
  /// orm model
  ormbr.model.master,
  ormbr.model.detail,
  ormbr.model.lookup,
  ormbr.model.client,
  ormbr.criteria.interfaces,
  /// Zeos
  ZAbstractConnection,
  ZConnection,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Param,
  FireDAC.Stan.Error,
  FireDAC.DatS,
  FireDAC.Phys.Intf,
  FireDAC.DApt.Intf,
  FireDAC.Stan.Async,
  FireDAC.DApt,
  FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TForm3 = class(TForm)
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    DBNavigator1: TDBNavigator;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    DBGrid2: TDBGrid;
    CDSDetail: TClientDataSet;
    DataSource2: TDataSource;
    CDSClient: TClientDataSet;
    DataSource3: TDataSource;
    CDSMaster: TClientDataSet;
    ZConnection1: TZConnection;
    CDSLookup: TClientDataSet;
    DBEdit1: TDBEdit;
    Label1: TLabel;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    FDMaster: TFDMemTable;
    FDDetail: TFDMemTable;
    FDClient: TFDMemTable;
    FDLookup: TFDMemTable;
    procedure Button3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
    oConn: IDBConnection;
    oMaster: IContainerDataSet<Tmaster>;
    oDetail: IContainerDataSet<Tdetail>;
    oClient: IContainerDataSet<Tclient>;
    oLookup: IContainerDataSet<Tlookup>;
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

{$R *.dfm}

procedure TForm3.Button2Click(Sender: TObject);
begin
  oMaster.DataSet.ApplyUpdates(0);
end;

procedure TForm3.Button3Click(Sender: TObject);
begin
  oMaster.DataSet.Open;
end;

procedure TForm3.Button4Click(Sender: TObject);
begin
  oMaster.DataSet.Close;
end;

procedure TForm3.FormCreate(Sender: TObject);
begin
  // Inst�ncia da class de conex�o via FireDAC
  oConn := TFactoryZeos.Create(ZConnection1, dnSQLite);

  /// Class Adapter
  /// Par�metros: (IDBConnection, TClientDataSet)
  oMaster := TContainerClientDataSet<Tmaster>.Create(oConn, CDSMaster, 10);
//  oMaster := TContainerFDMemTable<Tmaster>.Create(oConn, FDMaster, 10);

  /// Master-Detail
  oDetail := TContainerClientDataSet<Tdetail>.Create(oConn, CDSDetail, oMaster.DataSet);
//  oDetail := TContainerFDMemTable<Tdetail>.Create(oConn, FDDetail, oMaster.DataSet);

  /// Master-Lookup registro unico
  oClient := TContainerClientDataSet<Tclient>.Create(oConn, CDSClient, oMaster.DataSet);
//  oClient := TContainerFDMemTable<Tclient>.Create(oConn, FDClient, oMaster.DataSet);

  /// Lookup lista de registro (DBLookupComboBox)
  oLookup := TContainerClientDataSet<Tlookup>.Create(oConn, CDSLookup);
//  oLookup := TContainerFDMemTable<Tlookup>.Create(oConn, FDLookup);
  oDetail.DataSet.AddLookupField('lookup','lookup_id',oLookup.DataSet,'lookup_id','lookup_description');

  oMaster.DataSet.Open;
//  oMaster.DataSet.Open(10);
//  oMaster.DataSet.Open(ICriteria.SQL.Select.All.From('Master').OrderBy('description'));
end;

end.
