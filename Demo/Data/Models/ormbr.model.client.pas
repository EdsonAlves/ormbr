unit ormbr.model.client;

interface

uses
  Classes, 
  DB, 
  SysUtils, 
  Generics.Collections, 
  /// orm 
  ormbr.mapping.attributes, 
  ormbr.mapping.classes,
  ormbr.types.nullable,
  ormbr.types.lazy;

type
  [Entity]
  [Table('client','')]
  Tclient = class
  private
    { Private declarations } 
    Fclient_id: Integer;
    Fclient_name: String;
  public
    { Public declarations }
    [PrimaryKey()]
    [Column('client_id', ftInteger)]
    [Dictionary('client_id','Mensagem de valida��o','','','',taCenter)]
    property client_id: Integer Index 0 read Fclient_id write Fclient_id;

    [Column('client_name', ftString, 60)]
    [Indexe('idx_client_name','client_name')]
    [Dictionary('client_name','Mensagem de valida��o','','','',taLeftJustify)]
    property client_name: String Index 1 read Fclient_name write Fclient_name;
  end;

implementation

end.
